insert into payments (amount, user_id, transaction_id, status) values
(50,1,1,3),(75,1,2,3),(75,1,3,3),(150,1,4,3),(170,1,5,3), (200,2,6,3);

insert into orders (amount, user_id, movie_id) values
(200,1,1), (300,1,3);

insert into transactions (amount) values
(50),(75),(75),(150),(170),(200);