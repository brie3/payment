create table orders (
    id serial primary key,
    amount decimal(15,2) not null,
    user_id bigint(20) unsigned not null,
    movie_id bigint(20) unsigned not null,
    created_at int null
)engine=InnoDB;

create table payments (
    id serial primary key,
    amount decimal(15,2) not null,
    user_id bigint(20) unsigned not null,
    transaction_id bigint(20) unsigned not null,
    status tinyint null,
    created_at int null,
    updated_at int null
)engine=InnoDB;

create table transactions (
    id serial primary key,
    amount decimal(15,2) not null,
    status tinyint null,
    created_at int null
)engine=InnoDB;

create trigger t_cr_insert before insert on transactions
for each row begin
    set new.created_at = unix_timestamp(now());
    set new.status = 3;
end;

create trigger o_cr_insert before insert on orders
for each row begin
    set new.created_at = unix_timestamp(now());
end;

create trigger p_up_update before update on payments
for each row begin
    set new.updated_at = unix_timestamp(now());
end;

create trigger p_cr_up_insert before insert on payments
for each row begin
    if new.created_at is null
    then 
        set new.created_at = unix_timestamp(now());
    end if;
    
    if new.updated_at is null
    then
        set new.updated_at = unix_timestamp(now());
    end if;
end;