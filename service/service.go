// Package service implements mysql payment db protobuf interactions.
package service

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"os"
	"runtime/debug"
	"strconv"
	"time"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	internalFormat     = "internal error"
	insufficientFormat = "insufficient funds"
	errorFormat        = "[%s] error handled: %v"
	recoverFormat      = "[%s] recovered from %v, %s"
	key                = "x-request-id"

	createOrderQuery            = "insert into orders (amount, movie_id, user_id) values (?,?,?)"
	createOrderAmountErrFormat  = "invalid create order amount; want above zero"
	createOrderUserIDErrFormat  = "invalid create order userID"
	createOrderMovieIDErrFormat = "invalid create order movieID"
	amountFormat                = "%.2f"
	createOrderErrFormat        = "can't execute order creation query: %v"
	lastInsertedOrderErrFormat  = "can't get last inserted id on order creation: %v"

	createPaymentQuery           = "insert into payments (amount, user_id, transaction_id, status, created_at) values (?,?,?,?,?)"
	createTransactionQuery       = "insert into transactions (amount) values (?)"
	getTransactionQuery          = "select * from transactions where id = ?"
	createTransactionErrFormat   = "can't execute transaction creation query: %v"
	createPaymentErrFormat       = "can't execute payment creation query: %v"
	createPaymentAmountErrFormat = "invalid create payment amount; want above zero"
	createPaymentUserIDErrFormat = "invalid create payment userID"
	lastInsertedPaymentErrFormat = "can't get last inserted id on payment creation: %v"

	getOrderQuery     = "select * from orders where id = ?"
	getOrderErrFormat = "can't execute get order query: %v"

	getOrdersCount = "select count(*) from orders"
	getOrdersQuery = "select * from orders"
	movieId        = "movie_id = "

	getOrdersCountErrFormat = "can't execute count orders query: %v"
	getOrdersErrFormat      = "can't execute get orders query: %v"
	scanGetOrdersErrFormat  = "can't execute scan row for get orders query: %v"

	getPaymentQuery     = `select * from payments where id = ?`
	getPaymentErrFormat = "can't execute get payment query: %v"

	getPaymentsCount = "select count(*) from payments"
	getPaymentsQuery = "select * from payments"
	orderBy          = " order by user_id, id"
	limit            = " limit "
	userId           = "user_id = "
	comma            = ", "
	statusequals     = "status = "
	where            = " where "
	created          = "created_at between "
	and              = " and "

	getPaymentsCountErrFormat = "can't execute count payments query: %v"
	getPaymentsErrFormat      = "can't execute get payments query: %v"
	scanGetPaymentsErrFormat  = "can't execute scan row for get payments query: %v"

	getUserBalanceQuery = `select (coalesce((select sum(amount) 
	from payments where user_id = ?),0) - coalesce((select sum(amount) 
	from orders where user_id = ? ),0)) as balance`
	getBalanceErrFormat = "can't execute get balance query: %v"

	updatePaymentQuery     = `update payments set amount = ?, user_id = ?, transaction_id = ?, status = ? where id = ?`
	updatePaymentErrFormat = "can't execute update payment query: %v"
)

type Config struct {
	MaxOpenConns    int
	MaxIdleConns    int
	ConnectRetries  int
	PrometheusAddr  string
	ServiceAddr     string
	DBHost          string
	DBName          string
	DBUser          string
	DBPwd           string
	ConnMaxLifetime time.Duration
	SleepSeconds    time.Duration
}

// New brings to live new payment service.
func New(c *Config) (out *PaymentService, err error) {
	var pool *sql.DB
	if pool, err = dbConnect(c); err != nil {
		return
	}
	pool.SetConnMaxLifetime(c.ConnMaxLifetime)
	pool.SetMaxOpenConns(c.MaxOpenConns)
	pool.SetMaxIdleConns(c.MaxIdleConns)

	out = &PaymentService{db: pool}
	return
}

// PaymentService stands for payment interactions.
type PaymentService struct {
	db *sql.DB
}

func (p *PaymentService) Stop() error {
	return p.db.Close()
}

func wrapRecover(ctx context.Context, r interface{}) error {
	log.Printf(recoverFormat, getRid(ctx), r, debug.Stack())
	return status.Errorf(codes.Internal, internalFormat)
}

func wrapError(ctx context.Context, err error) error {
	log.Printf(errorFormat, getRid(ctx), err)
	return err
}

func getRid(ctx context.Context) string {
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if arr := md.Get(key); len(arr) > 0 {
			return arr[0]
		}
	}
	return ""
}

func dbConnect(conf *Config) (out *sql.DB, err error) {
	for i := 1; i < conf.ConnectRetries; i++ {
		if out, err = sql.Open("mysql",
			fmt.Sprintf("%s:%s@tcp(%s)/%s?parseTime=true&charset=utf8&interpolateParams=true&multiStatements=true&collation=utf8_general_ci&loc=",
				conf.DBUser, conf.DBPwd, conf.DBHost, conf.DBName),
		); err == nil {
			break
		}
		log.Printf("can't connect to payment db: %v", err)
		time.Sleep(time.Second * conf.SleepSeconds)
	}
	for i := 1; i < conf.ConnectRetries; i++ {
		if err = out.Ping(); err == nil {
			break
		}
		log.Printf("can't ping to payment db: %v", err)
		time.Sleep(time.Second * conf.SleepSeconds)
	}
	return
}

func SetupVars() (out *Config, err error) {
	var ok bool
	conf := &Config{}
	conf.PrometheusAddr, ok = os.LookupEnv("PROMETHEUS_ADDR")
	if !ok {
		log.Fatalf("PROMETHEUS_ADDR env is not set")
	}
	conf.ServiceAddr, ok = os.LookupEnv("SERVICE_ADDR")
	if !ok {
		log.Fatalf("SERVICE_ADDR env is not set")
	}
	conf.DBHost, ok = os.LookupEnv("DATABASE_HOST")
	if !ok {
		log.Fatalf("DATABASE_HOST env is not set")
	}
	conf.DBName, ok = os.LookupEnv("DATABASE_NAME")
	if !ok {
		log.Fatalf("DATABASE_NAME env is not set")
	}
	conf.DBUser, ok = os.LookupEnv("DATABASE_USER")
	if !ok {
		log.Fatalf("DATABASE_USER env is not set")
	}
	conf.DBPwd, ok = os.LookupEnv("DATABASE_PASSWORD")
	if !ok {
		log.Fatalf("DATABASE_PASSWORD env is not set")
	}

	var connectRetries, sleepSeconds string
	connectRetries, ok = os.LookupEnv("CONNECT_RETRIES")
	if tmp, err := strconv.Atoi(connectRetries); ok && err == nil {
		conf.ConnectRetries = tmp
	} else {
		conf.ConnectRetries = 12
	}

	sleepSeconds, ok = os.LookupEnv("SLEEP_SECONDS")
	if tmp, err := strconv.Atoi(sleepSeconds); ok && err == nil {
		conf.SleepSeconds = time.Duration(tmp)
	} else {
		conf.SleepSeconds = 15
	}
	conf.ConnMaxLifetime = time.Minute * 3
	conf.MaxOpenConns = 3
	conf.MaxIdleConns = 3
	return conf, nil
}

// RecoverInterceptor handles recovery from unexpected service behavior.
func RecoverInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (resp interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = wrapRecover(ctx, r)
		}
	}()
	return handler(ctx, req)
}
