package service

import (
	"context"
	pbPayment "payment/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetUserBalance gets balance for user.
func (p *PaymentService) GetUserBalance(
	ctx context.Context,
	req *pbPayment.GetUserBalanceRequest,
) (
	*pbPayment.GetUserBalanceResponse,
	error,
) {
	resp := &pbPayment.GetUserBalanceResponse{}
	if err := p.db.QueryRowContext(ctx, getUserBalanceQuery,
		req.GetUserId(), req.GetUserId()).Scan(
		&resp.Balance); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getBalanceErrFormat, err))
	}
	return resp, nil
}
