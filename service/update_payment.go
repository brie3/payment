package service

import (
	"context"
	pbPayment "payment/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// UpdatePayment updates payment in db.
func (p *PaymentService) UpdatePayment(
	ctx context.Context,
	req *pbPayment.UpdatePaymentRequest,
) (
	*pbPayment.Payment,
	error,
) {
	if _, err := p.db.ExecContext(ctx, updatePaymentQuery, req.GetAmount(), req.GetUserId(),
		req.GetTransactionId(), req.GetStatus(), req.GetId()); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, updatePaymentErrFormat, err))
	}
	return p.GetPayment(ctx, &pbPayment.PaymentRequest{Id: req.GetId()})
}
