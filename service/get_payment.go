package service

import (
	"context"
	pbPayment "payment/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetPayment gets payment.
func (p *PaymentService) GetPayment(
	ctx context.Context,
	req *pbPayment.PaymentRequest,
) (
	*pbPayment.Payment,
	error,
) {
	resp := &pbPayment.Payment{}
	if err := p.db.QueryRowContext(ctx, getPaymentQuery,
		req.GetId()).Scan(&resp.Id, &resp.Amount, &resp.UserId, &resp.TransactionId,
		&resp.Status, &resp.CreatedAt, &resp.UpdatedAt,
	); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getPaymentErrFormat, err))
	}
	return resp, nil
}
