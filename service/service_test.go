package service

import (
	"context"
	"database/sql"
	pbPayment "payment/proto"
	"testing"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/golang-migrate/migrate"
	"github.com/golang-migrate/migrate/database"
	"github.com/golang-migrate/migrate/database/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
)

func TestService(t *testing.T) {
	conf := &Config{
		MaxOpenConns:   3,
		MaxIdleConns:   3,
		ConnectRetries: 3,
		SleepSeconds:   3,
		DBHost:         "10.0.2.15", // TODO exclude explicit ip setup
		DBName:         "grpc",
		DBUser:         "db_user",
		DBPwd:          "db_pwd",
	}
	var (
		drv       database.Driver
		migration *migrate.Migrate
		pool      *sql.DB
		err       error
	)
	if pool, err = dbConnect(conf); err != nil {
		t.Error(err)
		return
	}

	defer func() {
		pool.Close()
	}()

	drv, err = mysql.WithInstance(pool, &mysql.Config{DatabaseName: conf.DBName})
	if err != nil {
		t.Error(err)
		return
	}

	migration, err = migrate.NewWithDatabaseInstance("file://../migrations", conf.DBName, drv)
	if err != nil {
		t.Error(err)
		return
	}
	if err = migration.Up(); err != nil && err != migrate.ErrNoChange {
		t.Error(err)
		return
	}
	pool.SetConnMaxLifetime(conf.ConnMaxLifetime)
	pool.SetMaxOpenConns(conf.MaxOpenConns)
	pool.SetMaxIdleConns(conf.MaxIdleConns)
	serv := &PaymentService{db: pool}

	defer func() {
		if err := migration.Down(); err != nil {
			t.Error(err)
		}
	}()

	ctx := context.Background()

	// CreatePayment tests
	createPaymentTest := func() {
		var cpr *pbPayment.CreatePaymentRequest
		for i := int32(1); i < 20; i++ {
			cpr = &pbPayment.CreatePaymentRequest{
				Amount: 1000,
				UserId: i,
				Status: pbPayment.PaymentStatus_PaymentStatus_NEW,
			}
			actual, err := serv.CreatePayment(ctx, cpr)
			if err != nil {
				t.Errorf("FAIL: CreatePayment\n%v\nActual:\n%v", cpr, err)
				return
			}
			if actual.GetAmount() != 1000 || actual.GetId() != i || actual.GetUserId() != i ||
				actual.GetStatus() != pbPayment.PaymentStatus_PaymentStatus_DEPOSITED {
				t.Errorf("FAIL: CreatePayment\n%v\nActual:\n%v", cpr, actual)
				return
			} else {
				t.Logf("PASS: CreatePayment test #%d", i)
			}
		}
	}

	createPaymentTest()

	// CreatePayment error tests
	createPaymentErrorTest := func() {
		tests := []struct {
			description string
			in          pbPayment.CreatePaymentRequest
		}{
			{"payment amount below zero", pbPayment.CreatePaymentRequest{Amount: -1000, UserId: 1, Status: pbPayment.PaymentStatus_PaymentStatus_NEW}},
			{"userID below zero", pbPayment.CreatePaymentRequest{Amount: 1000, UserId: 0, Status: pbPayment.PaymentStatus_PaymentStatus_NEW}},
		}
		for i := range tests {
			if actual, err := serv.CreatePayment(ctx, &tests[i].in); err == nil {
				t.Errorf("FAIL: CreatePaymentErrorTest\n%s\n%v\nActual:\n%v", tests[i].description, &tests[i].in, actual)
				return
			} else {
				t.Logf("PASS: CreatePaymentError error test #%d", 1)
			}
		}
	}

	createPaymentErrorTest()

	// CreateOrder tests
	createOrderTest := func() {
		var cor *pbPayment.CreateOrderRequest
		for i := int32(1); i < 20; i++ {
			cor = &pbPayment.CreateOrderRequest{
				Amount:  500,
				UserId:  i,
				MovieId: i,
			}
			actual, err := serv.CreateOrder(ctx, cor)
			if err != nil {
				t.Errorf("FAIL: CreateOrder\n%v\nActual:\n%v", cor, err)
				return
			}
			if actual.GetAmount() != 500 || actual.GetId() != i ||
				actual.GetUserId() != i || actual.GetMovieId() != i {
				t.Errorf("FAIL: CreateOrder\n%v\nActual:\n%v", cor, actual)
				return
			} else {
				t.Logf("PASS: CreateOrder test #%d", i)
			}
		}
	}

	createOrderTest()

	// CreateOrder error tests
	createOrderErrorTest := func() {
		tests := []struct {
			description string
			in          pbPayment.CreateOrderRequest
		}{
			{"order amount below zero", pbPayment.CreateOrderRequest{Amount: -1000, UserId: 1, MovieId: 1}},
			{"invalid userID", pbPayment.CreateOrderRequest{Amount: 500, UserId: 0, MovieId: 1}},
			{"invalid movieID", pbPayment.CreateOrderRequest{Amount: 500, UserId: 1, MovieId: 0}},
		}
		for i := range tests {
			if actual, err := serv.CreateOrder(ctx, &tests[i].in); err == nil {
				t.Errorf("FAIL: CreateOrderErrorTest\n%s\n%v\nActual:\n%v", tests[i].description, &tests[i].in, actual)
				return
			} else {
				t.Logf("PASS: CreateOrder error test #%d", 1)
			}
		}
	}

	createOrderErrorTest()

	// GetPayments tests
	getPaymentsTest := func() {
		var (
			pr       *pbPayment.PaymentsRequest
			from, to int64
		)
		for i := int32(0); i < 20; i++ {
			if i > 0 {
				from = time.Now().UTC().Unix() - 1000
				to = from + 2000
			}
			pr = &pbPayment.PaymentsRequest{
				Limit:         10,
				Offset:        0,
				UserId:        i,
				MovieId:       i,
				Status:        pbPayment.PaymentStatus_PaymentStatus_DEPOSITED,
				FromCreatedAt: from,
				ToCreatedAt:   to,
			}
			actual, err := serv.GetPayments(ctx, pr)
			if err != nil {
				t.Errorf("FAIL: GetPayments\n%v\nActual:\n%v", pr, err)
				return
			}
			for _, p := range actual.GetPayload() {
				if i != 0 && p.GetId() != i || i != 0 && p.GetUserId() != i || p.GetAmount() != 1000 ||
					p.GetStatus() != pbPayment.PaymentStatus_PaymentStatus_DEPOSITED ||
					from != 0 && from > p.GetCreatedAt() || to != 0 && p.GetCreatedAt() > to {
					t.Errorf("FAIL: GetPayments\n%v\nActual:\n%v", pr, p)
					return
				}
			}
			t.Logf("PASS: GetPayments test #%d", i+1)
		}
	}

	getPaymentsTest()

	// GetOrders tests
	getOrdersTest := func() {
		var (
			or       *pbPayment.OrdersRequest
			from, to int64
		)
		for i := int32(0); i < 20; i++ {
			if i > 0 {
				from = time.Now().UTC().Unix() - 1000
				to = from + 2000
			}
			or = &pbPayment.OrdersRequest{
				Limit:         10,
				Offset:        0,
				UserId:        i,
				MovieId:       i,
				FromCreatedAt: from,
				ToCreatedAt:   to,
			}
			actual, err := serv.GetOrders(ctx, or)
			if err != nil {
				t.Errorf("FAIL: GetOrders\n%v\nActual:\n%v", or, err)
				return
			}
			for _, p := range actual.GetPayload() {
				if i != 0 && p.GetId() != i || i != 0 && p.GetUserId() != i || p.GetAmount() != 500 ||
					from != 0 && from > p.GetCreatedAt() || to != 0 && p.GetCreatedAt() > to {
					t.Errorf("FAIL: GetOrders\n%v\nActual:\n%v", or, p)
					return
				}
			}
			t.Logf("PASS: GetOrders test #%d", i+1)
		}
	}

	getOrdersTest()

	// UpdatePayment tests
	updatePaymentTest := func() {
		var upr *pbPayment.UpdatePaymentRequest
		for i := int32(1); i < 20; i++ {
			upr = &pbPayment.UpdatePaymentRequest{
				Id:            i,
				Amount:        1000,
				UserId:        i,
				TransactionId: i,
				Status:        pbPayment.PaymentStatus_PaymentStatus_PENDING,
			}
			actual, err := serv.UpdatePayment(ctx, upr)
			if err != nil {
				t.Errorf("FAIL: UpdatePayment\n%v\nActual:\n%v", upr, err)
				return
			}
			if actual.GetAmount() != 1000 || actual.GetId() != i ||
				actual.GetUserId() != i || actual.GetTransactionId() != i ||
				actual.GetStatus() != pbPayment.PaymentStatus_PaymentStatus_PENDING {
				t.Errorf("FAIL: UpdatePayment\n%v\nActual:\n%v", upr, actual)
				return
			} else {
				t.Logf("PASS: UpdatePayment test #%d", i)
			}
		}
	}

	updatePaymentTest()
}
