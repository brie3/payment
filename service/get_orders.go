package service

import (
	"bytes"
	"context"
	pbPayment "payment/proto"
	"strconv"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetOrders gets orders.
func (p *PaymentService) GetOrders(ctx context.Context, req *pbPayment.OrdersRequest) (*pbPayment.Orders, error) {
	resp := &pbPayment.Orders{}
	query := &bytes.Buffer{}
	writeOrderCount(query, req)
	err := p.db.QueryRowContext(ctx, query.String()).Scan(&resp.Total)
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getOrdersCountErrFormat, err))
	}
	query.Reset()

	writeOrderQuery(query, req)
	rows, err := p.db.QueryContext(ctx, query.String())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getOrdersErrFormat, err))
	}
	for rows.Next() {
		po := pbPayment.Order{}
		if err = rows.Scan(&po.Id, &po.Amount, &po.UserId, &po.MovieId, &po.CreatedAt); err != nil {
			return nil, wrapError(ctx, status.Errorf(codes.Internal, scanGetOrdersErrFormat, err))
		}
		resp.Payload = append(resp.Payload, &po)
	}
	return resp, err
}

func writeOrderCount(w *bytes.Buffer, req *pbPayment.OrdersRequest) {
	w.WriteString(getOrdersCount)
	writeOrderStatements(w, req, true)
}

func writeOrderQuery(w *bytes.Buffer, req *pbPayment.OrdersRequest) {
	w.WriteString(getOrdersQuery)
	writeOrderStatements(w, req, true)
	w.WriteString(orderBy)
	writeOrderLimit(w, req)
}

func writeOrderStatements(w *bytes.Buffer, req *pbPayment.OrdersRequest, isFirst bool) {
	if req.GetUserId() == 0 && req.GetMovieId() == 0 &&
		req.GetFromCreatedAt() == 0 && req.GetToCreatedAt() == 0 {
		return
	}

	if isFirst {
		w.WriteString(where)
	}

	if req.GetUserId() != 0 {
		w.WriteString(userId + strconv.Itoa(int(req.GetUserId())))
		isFirst = false
	}

	if req.GetMovieId() != 0 {
		if !isFirst {
			w.WriteString(and)
		}
		w.WriteString(movieId + strconv.Itoa(int(req.GetMovieId())))
		isFirst = false
	}

	if req.GetFromCreatedAt() != 0 && req.GetToCreatedAt() != 0 {
		if !isFirst {
			w.WriteString(and)
		}
		w.WriteString(created + strconv.Itoa(int(req.GetFromCreatedAt())))
		w.WriteString(and + strconv.Itoa(int(req.GetToCreatedAt())))
	}
}

func writeOrderLimit(w *bytes.Buffer, req *pbPayment.OrdersRequest) {
	w.WriteString(limit)
	w.WriteString(strconv.Itoa(int(req.GetOffset())))
	w.WriteString(comma)
	w.WriteString(strconv.Itoa(int(req.GetLimit())))
}
