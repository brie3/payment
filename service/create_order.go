package service

import (
	"context"
	"database/sql"
	"fmt"
	pbPayment "payment/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// CreateOrder creates new order.
func (p *PaymentService) CreateOrder(
	ctx context.Context,
	req *pbPayment.CreateOrderRequest,
) (
	*pbPayment.Order,
	error,
) {
	if req.GetAmount() <= 0 {
		return nil, wrapError(ctx, status.Error(codes.InvalidArgument, createOrderAmountErrFormat))
	}

	if req.GetUserId() <= 0 {
		return nil, wrapError(ctx, status.Error(codes.InvalidArgument, createOrderUserIDErrFormat))
	}

	if req.GetMovieId() <= 0 {
		return nil, wrapError(ctx, status.Error(codes.InvalidArgument, createOrderMovieIDErrFormat))
	}

	ub, err := p.GetUserBalance(ctx, &pbPayment.GetUserBalanceRequest{UserId: req.UserId})
	if err != nil {
		return nil, err
	}
	if (ub.Balance - req.Amount) < 0 {
		return nil, wrapError(ctx, status.Errorf(codes.InvalidArgument, insufficientFormat))
	}
	var res sql.Result
	if res, err = p.db.ExecContext(ctx, createOrderQuery,
		fmt.Sprintf(amountFormat, req.GetAmount()), req.GetMovieId(), req.GetUserId()); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createOrderErrFormat, err))
	}
	id, err := res.LastInsertId()
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, lastInsertedOrderErrFormat, err))
	}
	return p.GetOrder(ctx, &pbPayment.OrderRequest{Id: int32(id)})
}
