package service

import (
	"context"
	"database/sql"
	pbPayment "payment/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type transaction struct {
	id        int32
	amount    float64
	status    int32
	createdAt int64
}

// CreatePayment creates payment.
func (p *PaymentService) CreatePayment(
	ctx context.Context,
	req *pbPayment.CreatePaymentRequest,
) (
	*pbPayment.Payment,
	error,
) {
	if req.GetAmount() < 0 {
		return nil, wrapError(ctx, status.Error(codes.InvalidArgument, createPaymentAmountErrFormat))
	}

	if req.GetUserId() <= 0 {
		return nil, wrapError(ctx, status.Error(codes.InvalidArgument, createPaymentUserIDErrFormat))
	}

	tr, err := p.createTransaction(req.GetAmount())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createTransactionErrFormat, err))
	}
	var res sql.Result
	if res, err = p.db.ExecContext(ctx, createPaymentQuery, tr.amount, req.GetUserId(),
		tr.id, tr.status, tr.createdAt); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createPaymentErrFormat, err))
	}
	id, err := res.LastInsertId()
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, lastInsertedPaymentErrFormat, err))
	}
	return p.GetPayment(ctx, &pbPayment.PaymentRequest{Id: int32(id)})
}

func (p *PaymentService) createTransaction(amount float32) (*transaction, error) {
	var (
		res sql.Result
		err error
	)
	if res, err = p.db.Exec(createTransactionQuery, amount); err != nil {
		return nil, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return nil, err
	}
	return p.getTransaction(int32(id))
}

func (p *PaymentService) getTransaction(id int32) (*transaction, error) {
	out := &transaction{}
	if err := p.db.QueryRow(getTransactionQuery, id).Scan(
		&out.id, &out.amount, &out.status, &out.createdAt); err != nil {
		return nil, err
	}
	return out, nil
}
