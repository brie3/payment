package service

import (
	"context"
	pbPayment "payment/proto"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetOrder gets order.
func (p *PaymentService) GetOrder(ctx context.Context, req *pbPayment.OrderRequest) (*pbPayment.Order, error) {
	po := pbPayment.Order{}
	if err := p.db.QueryRowContext(ctx, getOrderQuery, req.GetId()).Scan(
		&po.Id, &po.Amount, &po.UserId, &po.MovieId, &po.CreatedAt); err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getOrderErrFormat, err))
	}
	return &po, nil
}
