package service

import (
	"bytes"
	"context"
	pbPayment "payment/proto"
	"strconv"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetPayments gets payments.
func (p *PaymentService) GetPayments(
	ctx context.Context,
	req *pbPayment.PaymentsRequest,
) (
	*pbPayment.Payments,
	error,
) {
	query := &bytes.Buffer{}
	resp := &pbPayment.Payments{}
	writePaymentCount(query, req)
	err := p.db.QueryRowContext(ctx, query.String()).Scan(&resp.Total)
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getPaymentsCountErrFormat, err))
	}
	query.Reset()

	writePaymentQuery(query, req)
	rows, err := p.db.QueryContext(ctx, query.String())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, getPaymentsErrFormat, err))
	}
	for rows.Next() {
		pp := pbPayment.Payment{}
		if err = rows.Scan(&pp.Id, &pp.Amount, &pp.UserId, &pp.TransactionId,
			&pp.Status, &pp.CreatedAt, &pp.UpdatedAt); err != nil {
			return nil, wrapError(ctx, status.Errorf(codes.Internal, scanGetPaymentsErrFormat, err))
		}
		resp.Payload = append(resp.Payload, &pp)
	}
	return resp, nil
}

func writePaymentCount(w *bytes.Buffer, req *pbPayment.PaymentsRequest) {
	w.WriteString(getPaymentsCount)
	writePaymentStatements(w, req, true)
}

func writePaymentQuery(w *bytes.Buffer, req *pbPayment.PaymentsRequest) {
	w.WriteString(getPaymentsQuery)
	writePaymentStatements(w, req, true)
	w.WriteString(orderBy)
	writePaymentLimit(w, req)
}

func writePaymentStatements(w *bytes.Buffer, req *pbPayment.PaymentsRequest, isFirst bool) {
	if req.GetUserId() == 0 && req.Status == pbPayment.PaymentStatus_PaymentStatus_NOTSET &&
		req.GetFromCreatedAt() == 0 && req.GetToCreatedAt() == 0 {
		return
	}

	if isFirst {
		w.WriteString(where)
	}
	if req.GetUserId() != 0 {
		w.WriteString(userId + strconv.Itoa(int(req.GetUserId())))
		isFirst = false
	}

	if req.GetStatus() != pbPayment.PaymentStatus_PaymentStatus_NOTSET {
		if !isFirst {
			w.WriteString(and)
		}
		w.WriteString(statusequals + strconv.Itoa(int(req.GetStatus())))
		isFirst = false
	}

	if req.GetFromCreatedAt() != 0 && req.GetToCreatedAt() != 0 {
		if !isFirst {
			w.WriteString(and)
		}
		w.WriteString(created + strconv.Itoa(int(req.GetFromCreatedAt())))
		w.WriteString(and + strconv.Itoa(int(req.GetToCreatedAt())))
	}
}

func writePaymentLimit(w *bytes.Buffer, req *pbPayment.PaymentsRequest) {
	w.WriteString(limit)
	w.WriteString(strconv.Itoa(int(req.GetOffset())))
	w.WriteString(comma)
	w.WriteString(strconv.Itoa(int(req.GetLimit())))
}
