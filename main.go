package main

import (
	"log"
	"net"
	"net/http"
	pbPayment "payment/proto"
	"payment/service"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"google.golang.org/grpc"
)

func main() {
	conf, err := service.SetupVars()
	if err != nil {
		log.Fatal(err)
	}

	ps, err := service.New(conf)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		log.Fatal(ps.Stop())
	}()

	// prometheus
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		log.Fatal(http.ListenAndServe(conf.PrometheusAddr, nil))
	}()

	srv := grpc.NewServer(grpc.UnaryInterceptor(service.RecoverInterceptor))
	pbPayment.RegisterPaymentServiceServer(srv, ps)

	listener, err := net.Listen("tcp", conf.ServiceAddr)
	if err != nil {
		log.Fatalf("payment service failed to listen: %v", err)
	}

	log.Printf("starting payment service on %s", conf.ServiceAddr)
	log.Fatal(srv.Serve(listener))
}
